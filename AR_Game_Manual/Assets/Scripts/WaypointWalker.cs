using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointWalker : MonoBehaviour
{
    public Waypoint[] m_waypoints;

    public float m_stepSpeed;

    public float m_rotationSpeed;

    private int m_currentTargetIndex = 0;

    private Vector3 m_previousPosition;

    private Animator m_animController;

    private bool m_isMoving;

    [SerializeField]
    private int m_currentStepCount = 0;
    
    public void IncreaseStepCount(int steps)
    {  
        m_previousPosition = m_waypoints[m_currentTargetIndex - 1].transform.position;
        m_currentStepCount += steps;
    }

    private void StartFigureMovement()
    {
        LeanTween.move(this.gameObject, m_waypoints[m_currentTargetIndex].GetFigurePlacement(), m_stepSpeed).setOnComplete(OnTargetReached);

        Vector3 targetDirection = m_waypoints[m_currentTargetIndex].GetFigurePlacement() - transform.position;
        transform.rotation = Quaternion.LookRotation(targetDirection,Vector3.up);
        
        m_animController.speed = 1/m_stepSpeed;
        m_animController.SetBool("isMoving", true);
        m_isMoving = true;

    }

    private void OnTargetReached()
    {
        m_animController.SetBool("isMoving", false);
        m_isMoving = false;

        if (m_currentStepCount > 0)
        {
            m_currentStepCount--;   
        }
        if (m_currentStepCount < 0)
        {
            m_currentStepCount++;
        }

        if (m_currentTargetIndex < m_waypoints.Length - 1)
        {
            m_currentTargetIndex++;
        }
        else
        {
            m_currentTargetIndex = 0;
        }
    }

    public void StartSetup()
    {
        transform.position = m_waypoints[m_currentTargetIndex].GetFigurePlacement();
        m_currentTargetIndex++;
        Vector3 targetDirection = m_waypoints[m_currentTargetIndex].GetFigurePlacement() - transform.position;
        transform.rotation = Quaternion.LookRotation(targetDirection,Vector3.up);
    }

    private void Start()
    {
        LeanTween.reset();

        m_animController = GetComponentInChildren<Animator>(); 

        StartSetup();

    }


    private void Update()
    {
        
        if (m_currentStepCount != 0 !& m_isMoving == false)
        {
            StartFigureMovement();
        }
        else
        {
            m_animController.SetBool("isMoving", false);
        }

/*         if (Vector3.Distance(this.transform.position, m_waypoints[m_currentTargetIndex].GetFigurePlacement()) > 0)
        {
            float singleStep = m_rotationSpeed * Time.deltaTime;
            Vector3 targetDirection = m_waypoints[m_currentTargetIndex].GetFigurePlacement() - transform.position;
            transform.position = Vector3.MoveTowards(transform.position, m_waypoints[m_currentTargetIndex].GetFigurePlacement(), m_stepSpeed * Time.deltaTime);
            transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f));
            m_animController.SetBool("isMoving", true);
            //LeanTween.move(this.gameObject, m_waypoints[m_currentTargetIndex].GetFigurePlacement(), m_stepSpeed);
        }
        else
        {
            m_animController.SetBool("isMoving", false);
            if (m_currentStepCount != 0)
            {
                m_currentStepCount--;

                if (m_currentTargetIndex < m_waypoints.Length - 1)
                {
                    m_currentTargetIndex++;
                }
                else
                {
                    m_currentTargetIndex = 0;
                }
                
            }
        } */
    }

    public void SetStepCount(int targetStepCount)
    {
        this.m_currentStepCount = targetStepCount;
    }

}
