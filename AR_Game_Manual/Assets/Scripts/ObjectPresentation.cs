using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPresentation : MonoBehaviour
{
    public Camera m_camera;

    public bool isPresented = false;

    public float m_distance;

    public ObjectPresentationCamera m_presentationManager;

    [Header("Presentation Settings")]

    public bool m_dynamicPresentation = false;
    public float m_trailSpeed;

    public float m_rotationSpeed;


    private Vector3 m_previousPosition;

    private Vector3 m_previousRotation;

    private Vector3 m_previousScale;

    // Start is called before the first frame update
    void Start()
    {
        m_presentationManager = FindObjectOfType<ObjectPresentationCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPresented && m_presentationManager.m_currentObject == null)
        {
            m_presentationManager.RegisterObject(this.gameObject);
            SavePreviousTransform();
        }

        if (!isPresented && m_presentationManager.m_currentObject == this.gameObject)
        {
            m_presentationManager.UnregisterObject(this.gameObject);
        }    
    }

    private void UseStaticPresentation()
    {
        var currentCameraTransform = UpdateCameraTransform();

        transform.position = currentCameraTransform.position + currentCameraTransform.forward * m_distance;
    }

    private void UseDynamicPresentation()
    {
        var currentCameraTransform = UpdateCameraTransform();

        transform.position = Vector3.MoveTowards(this.transform.position, currentCameraTransform.position + currentCameraTransform.forward * m_distance, m_trailSpeed);
        transform.Rotate(0f, m_rotationSpeed, 0f, Space.Self); 

    }

    public void PresentObject(bool status)
    {
        isPresented = status;
    }

    private Transform UpdateCameraTransform()
    {
        var cameraTransform = m_camera.transform;

        return cameraTransform;
    }

    public void Present()
    {
        if (m_dynamicPresentation)
            {
                UseDynamicPresentation();
            }
        else
            {
                UseStaticPresentation();
            }
    }

    public void ReturnToPosition()
    {

        LeanTween.move(this.gameObject, m_previousPosition, 0.2f);
        LeanTween.rotate(this.gameObject, m_previousRotation, 0.2f);
        LeanTween.scale(this.gameObject, m_previousScale, 0.2f);
    }


    private void SavePreviousTransform()
    {
        m_previousPosition = transform.position;
        m_previousRotation = transform.rotation.eulerAngles;
        m_previousScale = transform.localScale;
    }


}
