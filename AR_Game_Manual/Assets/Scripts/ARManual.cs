using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using TMPro;
using System.Linq;
using static UnityEngine.ParticleSystem;

public class ARManual : MonoBehaviour
{

    [SerializeField]
    private int playerCount;

    public StateManager stateManager;
    public InitState initState;
    public SetupState setupState;
    public GameplayState gameplayState;

    public TMP_Text textObject;

    public GameObject presentedObject;

    [SerializeField]
    public List<StepData> stepData;
    public StepData currentData;

    public string[] stepTexts;

    public bool isDiceThrown;
    public int diceCount;
    public DiceController diceController;

    [SerializeField]
    public List<State> states = new List<State>();

    //Counter of executed sequences
    int stepSequences = 0;

    public int numberOfStates;

    public bool spacePressed = false;

    GameObject activeHighlighterObj;

    bool touchInteraction = false;

    /*
     * Data structure that contains all necessary steps,
     * with the name being the key for the step count
     */
    Dictionary<string, int> stepCounts = new Dictionary<string, int>()
    {
        {"init", 0},
        {"setup", 1},
        {"gameplay", 2}
    };

    Coroutine stateSequence;
    //private void Awake() {
        
    //}

    private void Awake() {
        DontDestroyOnLoad(transform.gameObject);
        Debug.Log("Init ARManual");
        stateManager = new StateManager();
        isDiceThrown = false;
        presentedObject = new GameObject();
        presentedObject.gameObject.AddComponent<ObjectPresentation>();

        //Initialize all States
        initState = new InitState(this, stateManager, stepCounts["init"]);
        setupState = new SetupState(this, stateManager, stepCounts["setup"]);
        gameplayState = new GameplayState(this, stateManager, stepCounts["gameplay"]);


        stateManager.Initialize(initState);
        SetStateText();
        SetStepData();
        states.Add(initState);
        states.Add(setupState);
        states.Add(gameplayState);
        //_state = stateManager._state;

        foreach (var x in states)
        {
            Debug.Log(x.ToString());
        }
        //StateSequence();

        //stateSequence = StartCoroutine(StateSequence());

    }

    private void Update()
    {
        //if (Input.GetMouseButtonDown(0) && touchInteraction == false)
        //{
        //    touchInteraction = true;
        //    StateSequence();
        //}
        //else if (Input.GetMouseButtonUp(0))
        //{
        //    touchInteraction = false;
        //}
        foreach (Touch touch in Input.touches)
        {

            if (touch.phase == TouchPhase.Began)
            {
                touchInteraction = true;
                StateSequence();
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                touchInteraction = false;
            }
        }
    }

    //Next sequence
    public void Next()
    {
        //StopCoroutine(stateSequence);
        Debug.Log("NEXT");
        stepSequences++;
        HandleStep();
        //Animation next
        //Show Object
        Debug.Log("Current State: " + stateManager.CurrentState.ToString());
        Debug.Log("Step Sequence: " + stepSequences.ToString());

        //StateSequence();
        //stateSequence = StartCoroutine(StateSequence());
    }

    public void HandleStep()
    {
        textObject.SetText(stepTexts[stepSequences]);
        if(currentData.presentedObjects[stepSequences] != presentedObject.name){
            //current one
            presentedObject.GetComponent<ObjectPresentation>().PresentObject(false);
            
            //new one
            if (currentData.presentedObjects[stepSequences] != "")
            {
                presentedObject = GameObject.Find(currentData.presentedObjects[stepSequences]);
                presentedObject.GetComponent<ObjectPresentation>().PresentObject(true);
            }
        }

        if (currentData.objectWalkDistance[stepSequences].Name.Length != 0 && currentData.objectWalkDistance[stepSequences].Name[0] != "")
        {
            string objName = currentData.objectWalkDistance[stepSequences].Name[0];
            GameObject moveObj = GameObject.Find(objName);
            int stepDistance = currentData.objectWalkDistance[stepSequences].distance[0];
            moveObj.GetComponent<WaypointWalker>().SetStepCount(stepDistance);
        }


        if (activeHighlighterObj != null)
        {
            activeHighlighterObj.SetActive(false);
        }
        //Activate Highlighter
        if (currentData.highlightedObject[stepSequences] != "")
        {
            string highlighterName = currentData.highlightedObject[stepSequences];
            activeHighlighterObj = GameObject.Find(highlighterName).transform.Find("Highlighter").gameObject;
            activeHighlighterObj.SetActive(true);
        }
    }

    //Next State -> Cleanup
    public void BeginState()
    {
        Debug.Log("START STATE");
        StopAllCoroutines();
        stepSequences = 0;
        int nextStep = stateManager.CurrentState.stepCount+=1;
        Debug.Log("StepCount is: " + nextStep.ToString());
        stateManager.TransitionTo(states[nextStep]);
        SetStepData();
        SetStateText();
        if (presentedObject)
        {
            presentedObject.GetComponent<ObjectPresentation>().PresentObject(false);
        }
        //Deactivate highlighter
        if (activeHighlighterObj != null)
        {
            activeHighlighterObj.SetActive(false);
        }
        //HandleStep();
        //stateSequence = StartCoroutine(StateSequence());
    }

    public void SetStepData()
    {
        int currentStep = stateManager.CurrentState.stepCount;
        this.currentData = stepData[currentStep];
        Debug.Log(currentData);
    }

    public void SetStateText()
    {
        this.stepTexts = stateManager.CurrentState.GetText();
        //This SetText is an inbuilt function
        textObject.SetText(stepTexts[stepSequences]);
    }

    public void SetPlayerCount(int count){
        this.playerCount = count;
    }

    //IEnumerator WaitForKeyDown()
    //{
    //    //while (!Input.GetKeyUp(keyCode))
    //    while (!touchInteraction)
    //        yield return null;
    //}

    public void StateSequence()
    {
        //yield return StartCoroutine(WaitForKeyDown());
        //yield return touchInteraction;

        //No more content in the State?
        if ((stepSequences >= currentData.texts.Length - 1) && (stepSequences >= currentData.presentedObjects.Length - 1))
        {
            BeginState();
            Debug.Log("Coroutine!");
        }
        else
        {
            Debug.Log("Still here");
            //if (currentData.diceValue[stepSequences] == 0)
            //{
            //    Debug.Log("No dice thrown");
            //}
            Next();
        }
        /*
         * while stepSequences != length of step data array
         *      -> startcoroutine(this) 
         */

    }

    //public IEnumerator StateSequence()
    //{
    //    yield return StartCoroutine(WaitForKeyDown());
    //    //yield return touchInteraction;

    //    //No more content in the State?
    //    if((stepSequences >= currentData.texts.Length-1) && (stepSequences >= currentData.presentedObjects.Length-1)) {
    //        BeginState();
    //        Debug.Log("Coroutine!");
    //    }
    //    else
    //    {
    //        Debug.Log("Still here");
    //        //if (currentData.diceValue[stepSequences] == 0)
    //        //{
    //        //    Debug.Log("No dice thrown");
    //        //}
    //        Next();
    //    }
    //    /*
    //     * while stepSequences != length of step data array
    //     *      -> startcoroutine(this) 
    //     */

    //}

    public void SetDiceCount(int count)
    {
        this.diceCount = count;
    }

}
