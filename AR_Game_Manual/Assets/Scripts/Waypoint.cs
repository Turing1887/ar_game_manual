using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    private bool[] m_positionStatusList;

    private int m_playerCount;

    public void SetPlayerCount(int count)
    {
        m_playerCount = count;
    }

    public Vector3 GetFigurePlacement()
    {
        var newPosition = transform.position;

        return newPosition;
    }


    private void Start()
    {
        SetPlayerCount(4);
        m_positionStatusList = new bool[4];
    }
}
