using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VuforiaTest : DefaultObserverEventHandler
{
    public GameObject myModelPrefab;
    public StateManager stateManager;

    private void Start()
    {
        stateManager = GameObject.Find("StateManager").GetComponent<StateManager>();
    }

    protected override void OnTrackingFound()
    {
        Debug.Log("Target Found");
    }

}
