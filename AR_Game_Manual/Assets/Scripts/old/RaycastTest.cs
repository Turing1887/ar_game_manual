using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;

public class RaycastTest : MonoBehaviour
{

    private ARRaycastManager raycastManager;
    public Text debugText;
    public Canvas overlay;

    protected float width;
    protected float height;

    // Start is called before the first frame update
    void Start()
    {
        width = (float)Screen.width / 2.0f;
        height = (float)Screen.height / 2.0f;
        raycastManager = FindObjectOfType<ARRaycastManager>();
        debugText.text = "POOPOO";
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount > 0)
        {
            Vector2 pos = Input.GetTouch(0).position;
            pos.x = (pos.x - width) / width;
            pos.y = (pos.y - height) / height;
            List<ARRaycastHit> hits = new List<ARRaycastHit>();
            //raycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, TrackableType.Planes);
            raycastManager.Raycast(pos, hits, TrackableType.Planes);

            if (hits.Count > 0)
            {
                overlay.transform.position = hits[0].pose.position;
                overlay.transform.rotation = hits[0].pose.rotation;
                debugText.text = hits[0].distance.ToString();
            }
        }
        
    }
}
