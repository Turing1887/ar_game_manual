using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SetupState : State
{
    //Concrete state

    public string _statetext = "Poopoo";
    public ARManual _manual;
    //public int stepCount;
    public int subSteps;

    //public StepData stepData = new StepData();
    public override string[] stateText { get => base.stateText; set => base.stateText = value; }

    public SetupState(ARManual manual, StateManager stateManager, int stepCount) : base(manual, stateManager, stepCount)
    {
        this._manual = manual;
        //this.stepCount = stepCount;
    }

    public override void HandleInput()
    {
        base.HandleInput();
        throw new System.NotImplementedException();
    }

    public override void SetGameBoard()
    {
        base.SetGameBoard();

        //initMono.Test();
        //SHOW INITIAL BUTTONS TO START APP
        Debug.Log("SETUPSTATE SETUP");
    }

    public override string[] GetText()
    {
        base.GetText();
        string stateName = _manual.stepData[1].texts[0];
        string[] stepTexts = _manual.stepData[1].texts;
        //stateText = stateName;
        return stepTexts;
        //Debug.Log(stuff);
    }
}
