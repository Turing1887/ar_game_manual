using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class State
{
    protected ARManual manual;
    protected StateManager stateManager;
    public int stepCount;
    public virtual string[] stateText { get; set; }
    protected State(ARManual manual, StateManager stateManager, int stepCount)
    {
        this.manual = manual;
        this.stateManager = stateManager;
        this.stepCount = stepCount;
        //this.stateText = stateText;
    }

    public virtual void HandleInput(){}
    public virtual void SetGameBoard(){}
    public virtual string[] GetText(){ return stateText; }

    //public virtual void SetContext(ARManual manual){
    //    this._manual = manual;
    //}
}
