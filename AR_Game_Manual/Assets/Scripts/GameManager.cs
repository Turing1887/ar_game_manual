using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class GameManager : MonoBehaviour
{
    public GameObject board;
    public ARPlane plane;
    private ARRaycastManager m_raycastManager;


    private void Start()
    {
        m_raycastManager = FindObjectOfType<ARRaycastManager>();
    }

    public void PlaceBoard(InputAction.CallbackContext context)
    {
        var touchPosition = context.ReadValue<Vector2>();
        List<ARRaycastHit> hits = new List<ARRaycastHit>();

        if (m_raycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
        {
            //raycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, TrackableType.Planes);
            var hitPose = hits[0].pose;
    
            board.transform.position = hits[0].pose.position;
            board.SetActive(true);
        }

    }
}
