using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StepData", menuName = "Steps/StepData", order = 1)]
public class StepData : ScriptableObject
{
    [SerializeField]
    [TextArea]
    public string[] texts;
    //[SerializeField]
    //public string[,] walkingObjects;
    //[SerializeField]
    //[TextArea]
    //[Header("Needs to correspond to the objects in the walkingObjects Array")]
    //public int[] objectDistance;
    [SerializeField]
    public string[] presentedObjects;

    //[SerializeField]
    //public Dictionary<int, GameObject> objectWalkDistance = new Dictionary<int, GameObject>();

    //[SerializeField]
    //public bool[] diceThrow;

    [SerializeField]
    public int[] diceValue;

    [Serializable]
    public struct ObjectWalkDistance
    {
        //Reference to the Object in the scene
        public string[] Name;
        //Fields it should walk
        public int[] distance;

        //Constructor (not necessary, but helpful)
        public ObjectWalkDistance(int[] distance, string[] Name)
        {
            this.Name = Name;
            this.distance = distance;
        }
    }
    [SerializeField]
    public ObjectWalkDistance[] objectWalkDistance;

    [Serializable]
    public struct ObjectAnimation
    {
        //Reference to the Object in the scene
        public string[] Name;
        //Fields it should walk
        public string[] animationName;

        //Constructor (not necessary, but helpful)
        public ObjectAnimation(string[] animationName, string[] Name)
        {
            this.Name = Name;
            this.animationName = animationName;
        }
    }
    [SerializeField]
    public ObjectAnimation[] objectAnimations;

    public string[] highlightedObject;

    //How many substeps does this state have?
    //public int subSteps;
}
