using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPresentationCamera : MonoBehaviour
{

    /* Diese Klasse regelt die Präsentation des Objekts  */


    public List<GameObject> m_objectList;
    
    public GameObject m_currentObject;


    public GameObject CheckCurrentObject()
    {
        if (m_currentObject = null)
        {
            return null;
        }
        else
        {
            return m_currentObject;
        }
    }

    public void RegisterObject(GameObject registeredObject)
    {
        if (m_currentObject == null)
        {
            m_currentObject = registeredObject;
        }
    }
    public void UnregisterObject(GameObject registeredObject)
    {
        if (m_currentObject = registeredObject)
        {
            registeredObject.GetComponent<ObjectPresentation>().ReturnToPosition();

            m_currentObject = null;
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        m_currentObject = null;
    }

    // Update is called once per frame
    void Update()
    {
        m_currentObject?.GetComponent<ObjectPresentation>().Present();
    }
    
}
