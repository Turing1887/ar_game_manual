using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARTrackedImageManager))]
public class TrackingManager : MonoBehaviour
{
    public Vector3 boardPos;
    public Vector3 eventFieldPos;
    public Vector3 commFieldPos;

    [SerializeField]
    [Tooltip("The camera to set on the world space UI canvas for each instantiated image info.")]
    Camera m_WorldSpaceCanvasCamera;

    [SerializeField]
    GameObject prefabObj;

    private bool cartAdded = false;

    /// <summary>
    /// The prefab has a world space UI canvas,
    /// which requires a camera to function properly.
    /// </summary>
    public Camera worldSpaceCanvasCamera
    {
        get { return m_WorldSpaceCanvasCamera; }
        set { m_WorldSpaceCanvasCamera = value; }
    }

    [SerializeField]
    [Tooltip("If an image is detected but no source texture can be found, this texture is used instead.")]
    Texture2D m_DefaultTexture;

    /// <summary>
    /// If an image is detected but no source texture can be found,
    /// this texture is used instead.
    /// </summary>
    public Texture2D defaultTexture
    {
        get { return m_DefaultTexture; }
        set { m_DefaultTexture = value; }
    }

    ARTrackedImageManager m_TrackedImageManager;

    void Awake()
    {
        m_TrackedImageManager = GetComponent<ARTrackedImageManager>();
    }

    void OnEnable()
    {
        m_TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }

    void OnDisable()
    {
        m_TrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }

    void SetPositions(Transform imageTransform)
    {
        boardPos = imageTransform.position;
        boardPos.y = boardPos.y + 0.05f;
        eventFieldPos = new Vector3(boardPos.x, boardPos.y, (boardPos.z + 0.5f));
        commFieldPos = new Vector3(boardPos.x, boardPos.y, (boardPos.z - 0.5f));
    }

    void AddObject(Vector3 pos, Transform parent)
    {
        GameObject newObject = Instantiate(prefabObj, pos, Quaternion.identity);
        //newObject.transform.localScale = new Vector3(0.01f, 1f, 0.01f);
        //newCart.transform.parent = parent;
    }

    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var trackedImage in eventArgs.added)
        {
            // Give the initial image a reasonable default scale
            trackedImage.transform.localScale = new Vector3(0.01f, 1f, 0.01f);
            if (trackedImage.referenceImage.name == "monopoly_3" && !cartAdded)
            {
                //var newCart = Instantiate(cart, trackedImage.transform.position, Quaternion.identity);
                //newCart.transform.parent = trackedImage.transform;
                SetPositions(trackedImage.transform.GetChild(0).transform);
                AddObject(boardPos, trackedImage.transform.GetChild(0).transform);
                cartAdded = true;
            }
        }
    }
}
