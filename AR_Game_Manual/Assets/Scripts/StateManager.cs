using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager
{
    
    //Reference to the current state
    public State CurrentState { get; private set; }

    //Initializes the first step
    public void Initialize(State startingState)
    {
        CurrentState = startingState;
        startingState.SetGameBoard();
        startingState.GetText();
    }

    public State GetState()
    {
        return CurrentState; 
    }

    //Transitions to the next step
    public void TransitionTo(State newState)
    {
        Debug.Log($"Context: Transition to {newState.GetType().Name}.");
        CurrentState = newState;
        newState.SetGameBoard();
        newState.GetText();
    }

}
