using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class SetupVisualizer : MonoBehaviour
{
    [SerializeField]
    GameObject[] playerPieces;
    int phases = 0;
    public void StartCycle(InputAction.CallbackContext context) {
        switch (phases)
        {
            case 0:
                StartCoroutine(PlacePlayerPieces());
                break;
            default:
                break;
        }
    }

    public IEnumerator PlacePlayerPieces()
    {
        foreach(GameObject piece in playerPieces)
        {
            GameObject newPiece = Instantiate(piece, new Vector3(0f, 0f, 0f), Quaternion.identity);
            newPiece.transform.parent = GameObject.FindGameObjectWithTag("Spielbrett").transform;
            newPiece.transform.position = new Vector3(0f, 0.05f, 0f);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void NextPhase()
    {
        StopAllCoroutines();
    }
}
