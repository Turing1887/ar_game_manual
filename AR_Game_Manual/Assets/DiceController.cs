using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceController : MonoBehaviour
{
    public Rigidbody[] m_rigidbody;
    public bool useGravity = false;

    private bool diceThrown = false;

    public ARManual manual;

    public GameObject dicePrefab;

    public GameObject gameBoardRef;

    public GameObject[] dice;

    public Vector3[] initialDicePos;

    void Awake()
    {
        //manual = GameObject.Find("GameManager").GetComponent<ARManual>();
        //gameBoardRef = GameObject.Find("Spielbrett");
        //dice = GameObject.Find("Dice");
        //dice = GameObject.FindGameObjectsWithTag("Dice");
        //for(int i = 0; i < dice.Length; i++)
        //{
        //    initialDicePos[i] = dice[i].transform.localPosition;
        //}
        Debug.Log(m_rigidbody.Length);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            //useGravity = true;
            //diceThrown = true;
            ThrowDice();
        }
    }

    public void ThrowDice()
    {
        foreach(Rigidbody m_body in m_rigidbody)
        {
            m_body.useGravity = false;

            m_body.AddForce(Physics.gravity * (m_body.mass * m_body.mass));

            float dirx = Random.Range(-500,500);
            float dirxy = Random.Range(-500,500);
            float dirz = Random.Range(-500,500);
            m_body.transform.localPosition = new Vector3(m_body.transform.position.x, 26f, m_body.transform.position.z);
            m_body.transform.rotation = Quaternion.identity;
            //m_rigidbody.AddForce(transform.up * 10);
            m_body.AddTorque(dirx,dirxy,dirz);
        }
        manual.SetDiceCount(2);
        manual.isDiceThrown = true;
        diceThrown = false;
        //SpawnDice();
        //dice.Set
    }
}
